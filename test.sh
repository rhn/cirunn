#!/bin/sh

set -e
set -v

SCRIPT=$(realpath $0)
DIR=$(dirname "${SCRIPT}")

cd "${DIR}"
# test 1
./cirunn.py --artifacts=arts ./test_cifile.yml # set up artifacts
./cirunn.py --artifacts=arts ./test_cifile.yml # overwrite artifacts (may write inside the dir)
./cirunn.py --artifacts=arts ./test_cifile.yml # overwrite artifacts and flake out if there's a dir inside already
