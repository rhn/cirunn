#!/usr/bin/python3

"""Cirunn runs GitLab CI files locally.

Requires podman.

License: AGPLv3.0
"""

import argparse
from dataclasses import dataclass
from datetime import datetime
import errno
import os
from pathlib import Path
import shlex
import shutil
import subprocess
import tempfile
import time
import yaml


# Top-level props that carry over to jobs
DEFAULT_PROPS = ('before_script', 'image')
# Forbidden job names
NON_JOB_PROPS = (*DEFAULT_PROPS, 'stages')


def copy2(src, dst, *, follow_symlinks=True):
    """Overwrites dst uncondtionally"""
    shutil.copyfile(src, dst, follow_symlinks=follow_symlinks)
    shutil.copystat(src, dst, follow_symlinks=follow_symlinks)
    return dst


def move_into(source, destination):
    """Moves contents of source into destination, replacing if needed."""
    if destination.exists():
        for node in source.iterdir():
            shutil.copytree(
                node,
                destination.joinpath(node.name),
                copy_function=copy2,
                dirs_exist_ok=True)
        shutil.rmtree(source)
    else:
        shutil.move(source, destination, copy_function=copy2)


@dataclass
class Job:
    name: str
    script: (str)
    image: str
    before_script: (str) = ()
    artifacts_paths: (str) = ()
    # TODO: stage, maybe


def get_defaults(contents):
    return [(name, value)
            for name, value in contents.items()
            if name in DEFAULT_PROPS]


def get_jobs(contents):
    defaults = get_defaults(contents)

    def fill_defaults(jobdesc):
        d = dict(defaults)
        d.update(jobdesc)
        return d

    jobs = ((name, fill_defaults(value))
            for name, value in contents.items()
            if name not in NON_JOB_PROPS)

    return [Job(name,
                value['script'], value['image'],
                value.get('before_script'),
                value.get('artifacts', {}).get('paths'))
            for name, value in jobs]


def run_job(job, overlay=True, container_name=None, artifacts_dir=None):
    retval = None

    extra_opts = []
    if container_name is None:
        job_container_name = 'cirunn-{}'.format(os.getpid())
        extra_opts.append('--rm')
    else:
        job_container_name = container_name

    if artifacts_dir is not None:
        extra_opts.extend(['-v', '{}:/artifacts:z'.format(shlex.quote(artifacts_dir.as_posix()))])

    with tempfile.NamedTemporaryFile(mode='w', prefix='cirunn-') as jobscript:
        def jsprint(*args):
            print(*args, file=jobscript)

        jsprint('set -e')
        # Without overlay, a copy of the incoming data must be made
        if overlay:
            jsprint('cd /mnt/sources')
        else:
            jsprint('cp -r /mnt/sources /sources')
            jsprint('cd /sources')

        for line in job.before_script + job.script:
            jsprint('echo', shlex.quote('>>>>>'), shlex.quote(line))
            jsprint(line)

        # Come back to the original directory
        if overlay:
            jsprint('cd /mnt/sources')
        else:
            jsprint('cd /sources')

        if artifacts_dir is not None and job.artifacts_paths is not None:
            for artifact in job.artifacts_paths:
                # FIXME: artifacts use globs, so they shouldn't be quoted.
                # But they shouldn't be allowed to executr anything they want.
                jsprint('cp -av ./{} /artifacts/'.format(artifact))
        # don't want the script to be incomplete to the container
        jobscript.flush()

        def run_process(cmd, *args, **kwargs):
            print(' '.join(map(shlex.quote, cmd)))
            return subprocess.Popen(cmd, *args, **kwargs)

        beginning = datetime.now()

        try:
            running = run_process(
                ['podman', 'run',
                 *extra_opts,
                 '--env', 'CI_PROJECT_DIR={}'.format('/mnt/sources' if overlay else '/sources'),
                 # Overlay mount this directory,
                 # so that files can be added,
                 # but don't need to be cleaned up outside the container
                 '-v', '.:/mnt/sources:' + ('O' if overlay else 'z'),
                 # make script file available
                 '-v', '{}:/mnt/script.sh:ro'.format(jobscript.name),
                 '--attach=stdout,stderr', # attach console to see progress
                 '--name=' + job_container_name,
                 '--workdir=/',
                 # This doesn't work because podman doesn't seem to see the mount
                 # https://github.com/containers/podman/issues/11352
                 # + ('/mnt/sources' if overlay else '/'),
                 
                 job.image,
                 'sh', '/mnt/script.sh', # execute the job script
                ],
                stdin=subprocess.PIPE)

            try:
                retval = running.wait(timeout=3600)
                end = datetime.now()
            except subprocess.TimeoutExpired:
                print('Job took too long. Killing.')
                running.terminate()
                time.sleep(10)
                if running.poll() is None:
                    running.kill()
                retval = 'KILLED'
            print('Job {} took {} time.'.format(job.name, end - beginning))
        except:
            print("KILLING")
            running.terminate()
            time.sleep(10)
            if running.poll() is None:
                running.kill()
            raise
        else:
            if container_name is not None:
                image_name = container_name.lower()
                subprocess.run(['podman', 'commit',
                                container_name, image_name],
                                check=True)
                print('Container preserved as image', image_name)
                print('To run, execute:')
                # FIXME: /sources is empty if no overlay
                print('    podman run --rm -ti {} {} /bin/bash'.format(
                    '-v .:/mnt/sources:O' if overlay else ''),
                    shlex.quote(image_name))
    return retval


def run_file(path, use_overlay, jobname=None, container_name=None, artifacts_dir=None):
    contents = yaml.safe_load(open(path).read())
    stages = contents.get('stages', [])
    jobs = get_jobs(contents)

    if artifacts_dir is not None:
        artifacts_dir = Path(artifacts_dir)
        artifacts_dir.mkdir(exist_ok=True, parents=True)
        """TODO: We use /tmp, but we risk putting lots of stuff in RAM.
        """
        temp_artifacts_dir = Path(tempfile.mkdtemp(prefix='cirunn'))
    else:
        temp_artifacts_dir = None

    if jobname is not None:
        jobs = filter(lambda x: x.name == jobname, jobs)

    results = []

    for job in jobs:
        print('running', job.name)
        kwargs = {}
        if container_name is not None:
            kwargs['container_name'] = container_name + '_' + job.name

        if temp_artifacts_dir is not None:
            job_artifacts_dir = temp_artifacts_dir.joinpath(job.name)
            job_artifacts_dir.mkdir()
            kwargs['artifacts_dir'] = job_artifacts_dir
        
        job.script = job.script or []
        job.before_script = job.before_script or []

        result = run_job(job, use_overlay, **kwargs)
        results.append((job.name, result))

    if temp_artifacts_dir is not None:
        move_into(temp_artifacts_dir, artifacts_dir)

    for (name, result) in results:
        print('Job', name,
            {
                None: 'did not execute.',
                'KILLED': 'took too long.',
                0: 'succeeded.',
            }.get(result, 'failed with code {}.'.format(result)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("cifile", help="The CI file to execute")
    parser.add_argument('--keep-as',
                        help='Keep the container after finishing, under the specified name. Job names will be appended.')
    parser.add_argument("--no-overlay",
                        action='store_true',
                        help="Don't mount source in overlay mode. Use this if podman complains about 'O' mount option.")
    parser.add_argument('--artifacts',
                        help='Directory to place artifacts. Job names will be subdirectories.')
    parser.add_argument('--job',
                        help='Execute job under this name')
    args = parser.parse_args()
    kwargs = {}
    if args.keep_as:
        kwargs['container_name'] = args.keep_as
    if args.artifacts:
        kwargs['artifacts_dir'] = args.artifacts
    if args.job:
        kwargs['jobname'] = args.job
    run_file(args.cifile, not args.no_overlay, **kwargs)
