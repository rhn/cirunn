CIRUNN
=====

Run your GitLab CI pipelines locally (or remotely) using podman.

cirunn.py
-----------

Executes `.gitlab-ci.yml` locally, and leaves the artifacts in a selected directory.

```
cd my_project/
cirunn.py ./gitlab-ci.yml --no-overlay --artifacts ../my_artifacts_dir
```

Keep in mind that local uncommitted changes will take effect - it's useful for intermediate testing.

ciorder.py
-----------

The same, but submit the job to your powerful build machine over the network.

```
cd my_project
ciorder.py builder@192.168.0.powerful ./gitlab-ci.tml --no-overlay --artifacts ../my_artifacts_dir
```

By default, the latest commit will be pushed to the remote host. Make sure the connection is fast enough to push the source and fetch the artifacts back.

This script needs *rsync* on both hosts, and podman only on the remote one.

Make sure to set up public key SSH login on the remote host, unless you like typing in passwords.

Cool details
-------------

Podman is running in user mode, which means that the container thinks it has root, but in reality, it doesn't! That saves us some security, and also password-typing.

`cirunn.py` has a `--keep-as` option, which makes it keep the container after a build. It can be useful for testing what went wrong, or for testing next commands to include in the job.
