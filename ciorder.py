#!/usr/bin/python3

"""Ciorder runs GitLab CI files remotely.

Requires podman, rsync.

License: AGPLv3.0
"""

import argparse
import os
import shlex
import subprocess


def run(cmd, *args, **kwargs):
    print(' '.join(map(shlex.quote, cmd)))
    return subprocess.run(cmd, check=True, *args, **kwargs)


def order_file(remote, cifile, use_overlay, use_git=False, artifacts=None):
    """Caution: this command may create the directory,
    but fail to communicate its name back.
    Don't try to read the output and act on that.
    It may be partial and removing / as part of clenup is not fun.
    """
    dirmaker = run(['ssh', remote, 'mktemp', '-d'],
                 capture_output=True,
                 text=True,
                 timeout=100)
    tmpdir = dirmaker.stdout.strip()
    srcdir = os.path.join(tmpdir, 'src')
    artifactdir = os.path.join(tmpdir, 'artifacts')
    try:
        script = '''cd {0} &&
{1} &&
cirunn ./.ciorder.yml {2} {3} &&
rm -rf {0}
        '''.format(shlex.quote(srcdir),
                   'git reset --hard' if use_git else 'echo',
                   '' if use_overlay else '--no-overlay',
                   '--artifacts {}'.format(shlex.quote(artifactdir))
                       if artifacts
                       else '')
        run(['rsync', '--compress', '-r',
             *(['./.git'] if use_git else ['--exclude=./.git', './']),
             '{}:{}'.format(remote, srcdir)],
            timeout=3600)
        run(['rsync', '--compress', '-r',
             cifile, '{}:{}/.ciorder.yml'.format(remote, srcdir)],
            timeout=300)
        def get_artifacts():
            if artifacts:
                run(['rsync', '--compress', '-r',
                     '{}:{}'.format(remote, artifactdir),
                     artifacts],
                    timeout=1800)              
        try:
            run(['ssh', remote],
                input=script,
                text=True,
                timeout=3600)
        except subprocess.CalledProcessError:
            get_artifacts()
            raise
        else:
            get_artifacts()
    except:
        run(['ssh', remote, 'rm', '-rf', srcdir],
            timeout=100)
        raise


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('remote', help="The remote ssh user@host to connect to")
    parser.add_argument("cifile", help="The CI file to execute")
    parser.add_argument("--no-overlay",
                        action='store_true',
                        help="Don't mount source in overlay mode. Use this if podman complains about 'O' mount option.")
    parser.add_argument("--no-git",
                        action='store_true',
                        help="Don't copy the git repo, but the contents of the current directory instead. Use this when a dirty checkout is needed, or when sshfs fails due to git.")
    parser.add_argument('--artifacts',
                        help='Directory to place artifacts. Job names will be subdirectories.')
    
    args = parser.parse_args()
    order_file(args.remote, args.cifile, not args.no_overlay, not args.no_git, args.artifacts)
